import { TransactionPack } from '../../src/TransactionPack';
import { remoteCommandHandler } from './helpers';
import { UMI } from '../../src/UMI';

jest.mock('child_process');

describe('TransactionPack', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(async () => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.universa.contract.TransactionPack',
        id: req.id,
      };
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should create an instance of TransactionPack', async () => {
    const transactionPack = await TransactionPack.create();
    expect(transactionPack).toBeInstanceOf(TransactionPack);
  });

  it('should create an instance of TransactionPack via UMI.instantiate', async () => {
    const transactionPack = await UMI.instantiate('TransactionPack');
    expect(transactionPack).toBeInstanceOf(TransactionPack);
  });
});
