import { ContractState } from '../../src/ContractState';
import { ItemStates } from '../../src/types';

describe('ContractState', () => {
  it('should create an instance of ContractState', () => {
    const contractState = new ContractState({ state: ItemStates.APPROVED });
    expect(contractState).toBeInstanceOf(ContractState);
    expect(contractState.state).toBe('APPROVED');
    expect(contractState.approved).toBe(true);
    expect(contractState.isApproved).toEqual(contractState.approved);
    expect(contractState.hasErrors).toBe(false);
  });

  it('should create a ContractState with errors', () => {
    const contractState = new ContractState({ state: ItemStates.DECLINED, errors: ['error'] });
    expect(contractState.hasErrors).toBe(true);
    expect(contractState.errors).toEqual(['error']);
  });

  it('should create a pending ContractState', () => {
    const contractState = new ContractState({ state: ItemStates.PENDING_POSITIVE });
    expect(contractState.pending).toBe(true);
    expect(contractState.isPending).toEqual(contractState.pending);
  });
});
