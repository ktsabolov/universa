import { Connection } from '../../src/Connection';
import { UmiClient } from '../../src/UmiClient';

jest.mock('child_process');

describe('Connection', () => {
  it('should create an instance of Connection', async () => {
    const client = {} as UmiClient;
    const connection = new Connection(client);
    expect(connection).toBeInstanceOf(Connection);
  });

  it('should return the node number', async () => {
    const client = {} as UmiClient;
    client.getNodeNumber = jest.fn(async () => 5);
    const connection = new Connection(client);
    await expect(connection.getNodeNumber()).resolves.toBe(5);
    expect(client.getNodeNumber).toBeCalledTimes(1);
    await connection.getNodeNumber();
    expect(client.getNodeNumber).toBeCalledTimes(1);
  });

  it('should ping the node', async () => {
    const client = {} as UmiClient;
    client.ping = async () => true;
    const connection = new Connection(client);
    await expect(connection.ping()).resolves.toBe(true);
  });

  it('should ping the node', async () => {
    const client = {} as UmiClient;
    client.restart = async () => {};
    const connection = new Connection(client);
    await expect(connection.restart()).resolves.toBe(undefined);
  });

  it("should return the node's url", async () => {
    const client = {} as UmiClient;
    client.getUrl = jest.fn(async () => 'https://node.tld');
    const connection = new Connection(client);
    await expect(connection.getUrl()).resolves.toBe('https://node.tld');
    expect(client.getUrl).toBeCalledTimes(1);
    await connection.getUrl();
    expect(client.getUrl).toBeCalledTimes(1);
  });
});
