import { EventEmitter } from 'events';
import { remoteCommandHandler } from '../helpers';

export function spawn() {
  const proc = new EventEmitter() as any;
  proc.stdin = new EventEmitter() as any;
  proc.stdout = new EventEmitter();
  proc.unref = () => {};
  proc.pid = 'mock';
  proc.kill = (signal: string) => {
    signal = signal || 'SIGTERM';
    if (!proc.killed) {
      proc.killed = true;
      process.nextTick(() => {
        const code = signal === 'SIGINT' ? 130 : 143;
        proc.emit('exit', code, null);
      });
    }
  };

  let remoteSerial = 0;
  let remoteObjectId = 1;

  proc.stdin.write = function(data: string) {
    const { serial, ...request } = JSON.parse(data.split('\n')[0]);
    const response: { serial: number; ref: number; result?: any; error?: any } = {
      serial: remoteSerial++,
      ref: serial,
    };

    Object.defineProperty(request, 'id', { get: () => remoteObjectId++ });

    let promise = Promise.resolve();
    if (request.cmd === 'version') {
      response.result = { system: 'UMI', version: '0.8.63', protocol: '0.0.3' };
    } else {
      promise = remoteCommandHandler(request, response);
    }

    promise.then(() => {
      proc.stdout.emit('data', Buffer.from(JSON.stringify(response)));
    });
  };

  return proc;
};
