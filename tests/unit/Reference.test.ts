import { Reference } from '../../src/Reference';
import { remoteCommandHandler } from './helpers';

jest.mock('child_process');

describe('Reference', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(async () => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.universa.contract.Reference',
        id: req.id,
      };
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should create an instance of ModifyDataPermission', async () => {
    const reference = await Reference.create();
    expect(reference).toBeInstanceOf(Reference);
  });
});
