import { UmiClient } from '../../src/UmiClient';
import { remoteCommandHandler } from './helpers';

jest.mock('child_process');

describe('UmiClient', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(() => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.universa.node2.network.Client',
        id: req.id,
      };
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should create an instance of UmiClient', async () => {
    const client = await UmiClient.create();
    expect(client).toBeInstanceOf(UmiClient);
  });

  it('should make only one UMI call for size', async () => {
    remoteCommandHandler.mockImplementationOnce(async (_, res: any) => {
      res.result = 33;
    });
    const client = await UmiClient.create();
    const spy = jest.spyOn(client, 'invoke');
    await expect(client.getSize()).resolves.toBe(33);
    expect(spy).toBeCalledTimes(1);
    await client.getSize();
    expect(spy).toBeCalledTimes(1);
  });
});
