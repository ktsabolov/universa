import { Client } from '../../src/Client';
import { PrivateKey } from '../../src/PrivateKey';
import { remoteCommandHandler } from './helpers';
import { Connection } from '../../src/Connection';

jest.mock('child_process');

describe('Client', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(async () => {
    remoteCommandHandler
      .mockImplementationOnce(async (req: any, res: any) => {
        res.result = {
          __type: 'RemoteObject',
          className: 'com.icodici.crypto.PrivateKey',
          id: req.id,
        };
      })
      .mockImplementationOnce(async (req: any, res: any) => {
        res.result = {
          __type: 'RemoteObject',
          className: 'com.icodici.universa.node2.network.Client',
          id: req.id,
        };
      })
      .mockImplementationOnce(async (_, res: any) => {
        res.result = 33;
      });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should create an instance of Client', async () => {
    const client = await Client.create();
    expect(client).toBeInstanceOf(Client);
  });

  it('should create an instance of Client with existing privateKey', async () => {
    const privateKey = await PrivateKey.create(2048);
    const client = await Client.create({ privateKey });
    expect(client).toBeInstanceOf(Client);
  });

  it('should throw when constructor is called directly', () => {
    expect(() => {
      new Client({} as any);
    }).toThrowError(`Constructor of class 'Client' is private and only accessible within the class declaration.`);
  });

  it("should return a connection to specific node by it's number", async () => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.universa.node2.network.Client',
        id: req.id,
      };
    });

    const client = await Client.create();
    const connection = await client.get(5);
    const connection2 = await client.get(5);
    expect(connection).toBeInstanceOf(Connection);
    expect(connection2).toBe(connection);

    expect(client.get(-1)).rejects.toThrow();
    expect(client.get(34)).rejects.toThrow();
  });

  it('should get a random connection', async () => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.universa.node2.network.Client',
        id: req.id,
      };
    });

    const client = await Client.create();
    const connection = await client.getRandomConnection();
    expect(connection).toBeInstanceOf(Connection);
  });

  it('should get N random connections', async () => {
    remoteCommandHandler.mockImplementation(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.universa.node2.network.Client',
        id: req.id,
      };
    });

    const client = await Client.create();
    const connections = await client.getRandomConnections(4);
    expect(connections).toHaveLength(4);
    expect(connections.every((c) => c instanceof Connection)).toBe(true);

    expect(client.getRandomConnections(0)).rejects.toThrow();
    expect(client.getRandomConnections(34)).rejects.toThrow();
  });
});
