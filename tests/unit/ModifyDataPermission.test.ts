import { ModifyDataPermission } from '../../src/ModifyDataPermission';
import { remoteCommandHandler } from './helpers';

jest.mock('child_process');

describe('ModifyDataPermission', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(async () => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.universa.contract.permissions.ModifyDataPermission',
        id: req.id,
      };
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should create an instance of ModifyDataPermission', async () => {
    const modifyDataPermission = await ModifyDataPermission.create();
    expect(modifyDataPermission).toBeInstanceOf(ModifyDataPermission);
  });
});
