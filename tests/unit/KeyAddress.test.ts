import { KeyAddress } from '../../src/KeyAddress';
import { RemoteObject } from '../../src/RemoteObject';
import { remoteCommandHandler } from './helpers';

jest.mock('child_process');

describe('KeyAddress', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(() => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.crypto.KeyAddress',
        id: req.id,
      };
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should instantiate an instance of KeyAddress', async () => {
    const keyAddress = await KeyAddress.create();
    expect(keyAddress).toBeInstanceOf(KeyAddress);
  });

  it("should instantiate with 'fromPacked'", async () => {
    const buf = Buffer.from('string');
    const spy = jest.spyOn(RemoteObject, 'create');
    const keyAddress = await KeyAddress.fromPacked(buf);
    expect(keyAddress).toBeInstanceOf(KeyAddress);
    expect(spy).toBeCalledTimes(2);
    expect(spy).toBeCalledWith(buf);
  });

  it('should return string representation of KeyAddress', async () => {
    remoteCommandHandler.mockImplementationOnce(async (_, res: any) => {
      res.result = 'keyAddress';
    });
    const keyAddress = (await KeyAddress.create()) as any;
    const spy = jest.spyOn(keyAddress, 'invoke');
    const string = await keyAddress.toString();
    expect(string).toEqual('keyAddress');
    expect(spy).toBeCalledTimes(1);
    await keyAddress.toString();
    expect(spy).toBeCalledTimes(1);
  });

  it('should check for equality two key addresses', async () => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.crypto.KeyAddress',
        id: req.id,
      };
    });

    const keyAddress1 = (await KeyAddress.create()) as any;
    const keyAddress2 = (await KeyAddress.create()) as any;

    let packedLength = 37;
    remoteCommandHandler.mockImplementation(async (req: any, res: any) => {
      if (req.cmd === 'invoke') {
        if (req.args[1] === 'getPacked') {
          res.result = new Array(packedLength).fill('-').join('');
          packedLength = packedLength === 37 ? 53 : 37;
        } else if (req.args[1] === 'getString') {
          res.result = 'keyAddress' + req.args[0];
        }
      }
    });

    expect(await keyAddress1.equals(keyAddress1)).toBe(true);
    expect(await keyAddress1.equals(keyAddress2)).toBe(false);
    expect(await keyAddress1.equals('keyAddress' + keyAddress1._ref.id));
    expect(await keyAddress1.equals('-------------------------------------')).toBe(true);
    expect(await keyAddress1.equals('-----------------------------------------------------')).toBe(true);
  });
});
