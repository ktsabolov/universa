import { KeyAddress } from '../../src/KeyAddress';
import { PrivateKey } from '../../src/PrivateKey';
import { PublicKey } from '../../src/PublicKey';
import { RemoteObject } from '../../src/RemoteObject';
import { remoteCommandHandler } from './helpers';

jest.mock('child_process');

describe('PrivateKey', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(() => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.crypto.PrivateKey',
        id: req.id,
      };
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should create an instance of PrivateKey', async () => {
    const spy = jest.spyOn(RemoteObject, 'create');
    const privateKey = await PrivateKey.create(2048);
    expect(privateKey).toBeInstanceOf(PrivateKey);
    expect(spy).toBeCalledTimes(2);
    expect(spy).toBeCalledWith(2048);
  });

  it("should instantiate with 'fromPacked'", async () => {
    const spy = jest.spyOn(RemoteObject, 'create');
    const buf = Buffer.from('string');
    const privateKey = await PrivateKey.fromPacked(buf);
    expect(privateKey).toBeInstanceOf(PrivateKey);
    expect(spy).toBeCalledTimes(2);
    expect(spy).toBeCalledWith(buf);
  });

  it("should instantiate with 'fromPacked' protected by password", async () => {
    const spy = jest.spyOn(RemoteObject, 'invoke');
    const buf = Buffer.from('string');
    const privateKey = await PrivateKey.fromPacked(buf, 'password');
    expect(privateKey).toBeInstanceOf(PrivateKey);
    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith('unpackWithPassword', buf, 'password');
  });

  describe('public key', () => {
    beforeEach(() => {
      remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
        res.result = {
          __type: 'RemoteObject',
          className: 'com.icodici.crypto.PublicKey',
          id: req.id,
        };
      });
    });

    it('should return a public key', async () => {
      const privateKey = (await PrivateKey.create(2048)) as any;
      const spy = jest.spyOn(privateKey, 'invoke');
      const publicKey = await privateKey.getPublicKey();
      expect(publicKey).toBeInstanceOf(PublicKey);
      expect(privateKey._publicKey).toStrictEqual(publicKey);
      expect(spy).toBeCalledTimes(1);
      expect(spy).toBeCalledWith('getPublicKey');
      await privateKey.getPublicKey();
      expect(spy).toBeCalledTimes(1);
    });

    it('should return a short address', async () => {
      remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
        res.result = {
          __type: 'RemoteObject',
          className: 'com.icodici.crypto.KeyAddress',
          id: req.id,
        };
      });

      const privateKey = (await PrivateKey.create(2048)) as any;
      await privateKey.getPublicKey();
      const spy = jest.spyOn(privateKey._publicKey, 'getShortAddress');
      const shortAddress = await privateKey.getShortAddress();
      expect(shortAddress).toBeInstanceOf(KeyAddress);
      expect(privateKey._shortAddress).toStrictEqual(shortAddress);
      expect(spy).toBeCalledTimes(1);
      await privateKey.getShortAddress();
      expect(spy).toBeCalledTimes(1);
    });

    it('should return a long address', async () => {
      remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
        res.result = {
          __type: 'RemoteObject',
          className: 'com.icodici.crypto.KeyAddress',
          id: req.id,
        };
      });
      const privateKey = (await PrivateKey.create(2048)) as any;
      await privateKey.getPublicKey();
      const spy = jest.spyOn(privateKey._publicKey, 'getLongAddress');
      const longAddress = await privateKey.getLongAddress();
      expect(longAddress).toBeInstanceOf(KeyAddress);
      expect(privateKey._longAddress).toStrictEqual(longAddress);
      expect(spy).toBeCalledTimes(1);
      await privateKey.getLongAddress();
      expect(spy).toBeCalledTimes(1);
    });

    it('should return a bit strength', async () => {
      remoteCommandHandler.mockImplementationOnce(async (_, res: any) => {
        res.result = 2048;
      });
      const privateKey = (await PrivateKey.create(2048)) as any;
      await privateKey.getPublicKey();
      const spy = jest.spyOn(privateKey._publicKey, 'getBitStrength');
      const bitStrength = await privateKey.getBitStrength();
      expect(bitStrength).toStrictEqual(2048);
      expect(privateKey._bitStrength).toStrictEqual(bitStrength);
      expect(spy).toBeCalledTimes(1);
      await privateKey.getBitStrength();
      expect(spy).toBeCalledTimes(1);
    });
  });

  it('should sign a string with the key', async () => {
    const expectedResult = Buffer.from('signed string').toString('base64');

    remoteCommandHandler.mockImplementation(async (_, res: any) => {
      res.result = expectedResult;
    });

    const privateKey = (await PrivateKey.create(2048)) as any;
    const spy = jest.spyOn(privateKey, 'invoke');
    const buf = Buffer.from('string');
    const signed = await privateKey.sign(buf);
    expect(signed).toStrictEqual(expectedResult);
    expect(spy).toBeCalledTimes(1);
    expect(spy).toBeCalledWith('sign', buf, 'SHA3_384');
    await privateKey.sign('string');
    expect(spy).toBeCalledWith('sign', Buffer.from('string'), 'SHA3_384');
    await privateKey.sign(buf, 'SHA2');
    expect(spy).toBeCalledWith('sign', buf, 'SHA2');
  });
});
