import { RevokePermission } from '../../src/RevokePermission';
import { remoteCommandHandler } from './helpers';

jest.mock('child_process');

describe('RevokePermission', () => {
  afterAll(() => {
    jest.unmock('child_process');
    jest.restoreAllMocks();
  });

  beforeEach(async () => {
    remoteCommandHandler.mockImplementationOnce(async (req: any, res: any) => {
      res.result = {
        __type: 'RemoteObject',
        className: 'com.icodici.universa.contract.permissions.RevokePermission',
        id: req.id,
      };
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should create an instance of ModifyDataPermission', async () => {
    const revokePermission = await RevokePermission.create();
    expect(revokePermission).toBeInstanceOf(RevokePermission);
  });
});
