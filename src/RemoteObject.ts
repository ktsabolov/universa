import { PrivateConstructorError } from './errors';
import { RemoteObjectRef } from './RemoteObjectRef';
import { JsValue, UmiRef } from './types';
import { UMI } from './UMI';
import { getObjectMethods, hasProperty } from './util';

const INTERNAL_CALL = Symbol('internalCall');

export class RemoteObject {
  private static knownProxies: Map<string, typeof RemoteObject> = new Map();
  protected static readonly remoteClassName: string;
  protected static localProperties: string[] = ['then'];
  private _ref: RemoteObjectRef;

  public static registerProxy(proxyConstructor: typeof RemoteObject) {
    if (this.knownProxies.get(proxyConstructor.remoteClassName)) {
      throw new Error(`Proxy for '${proxyConstructor.remoteClassName}' already registered`);
    }
    this.knownProxies.set(proxyConstructor.remoteClassName, proxyConstructor);
    return true;
  }

  public static async create(remoteObject: RemoteObjectRef): Promise<RemoteObject>;
  public static async create(...args: any[]): Promise<RemoteObject>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<RemoteObject> {
    if (args.length === 1 && args[0] instanceof RemoteObjectRef) {
      const remoteObjectRef = args[0] as RemoteObjectRef;
      const remoteClassName = remoteObjectRef.remoteClassName;
      if (remoteClassName && this.knownProxies.has(remoteClassName)) {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        const RemoteObjectProxy = this.knownProxies.get(remoteClassName)!;
        return new RemoteObjectProxy(INTERNAL_CALL, args[0]);
      }
      return new this(INTERNAL_CALL, args[0]);
    } else if (this.remoteClassName) {
      return UMI.instantiate(this.remoteClassName, ...args);
    } else {
      throw new Error(
        `'${this.name}' could not be created directly because 'remoteClassName' property is not specified on the class`,
      );
    }
  }

  public static invoke(method: string, ...args: any[]): Promise<JsValue> {
    return UMI.invoke(this.remoteClassName, method, ...args);
  }

  protected constructor(internalCall: symbol, ref: RemoteObjectRef) {
    if (internalCall !== INTERNAL_CALL) throw new PrivateConstructorError(this.constructor.name);

    this._ref = ref;

    const localMethods = getObjectMethods(this, RemoteObject);

    return new Proxy(this, {
      get: (target, property) => {
        const targetPrototype = Reflect.getPrototypeOf(target);
        const targetConstructor = targetPrototype.constructor as typeof RemoteObject;

        if (
          localMethods.includes(property as string) ||
          targetConstructor.localProperties.includes(property as string) ||
          hasProperty(target, property) ||
          typeof property !== 'string'
        ) {
          return Reflect.get(target, property);
        }

        return function(...args: any[]): Promise<JsValue> {
          return target.invoke(property as string, ...args);
        };
      },
    });
  }

  /* eslint-disable @typescript-eslint/no-non-null-assertion */
  public get umi(): UMI {
    return this._ref.umi;
  }

  public getRef(umi: UMI): UmiRef {
    return this._ref.getRef(umi);
  }

  public get ref(): RemoteObjectRef {
    return this._ref;
  }
  /* eslint-enable @typescript-eslint/no-non-null-assertion */

  protected async invoke(method: string, ...args: JsValue[]): Promise<JsValue> {
    return this.umi.invoke(this.ref, method, ...args);
  }
}
