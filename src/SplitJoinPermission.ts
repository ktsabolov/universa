import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class SplitJoinPermission extends RemoteObject {
  protected static readonly remoteClassName = 'com.icodici.universa.contract.permissions.SplitJoinPermission';

  public static async create(remoteObject: RemoteObjectRef): Promise<SplitJoinPermission>;
  public static async create(...args: any[]): Promise<SplitJoinPermission>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<SplitJoinPermission> {
    return super.create.call(this, ...args) as Promise<SplitJoinPermission>;
  }
}

RemoteObject.registerProxy(SplitJoinPermission);
