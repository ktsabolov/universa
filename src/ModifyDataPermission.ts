import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class ModifyDataPermission extends RemoteObject {
  protected static readonly remoteClassName = 'com.icodici.universa.contract.permissions.ModifyDataPermission';

  public static async create(remoteObject: RemoteObjectRef): Promise<ModifyDataPermission>;
  public static async create(...args: any[]): Promise<ModifyDataPermission>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<ModifyDataPermission> {
    return super.create.call(this, ...args) as Promise<ModifyDataPermission>;
  }
}

RemoteObject.registerProxy(ModifyDataPermission);
