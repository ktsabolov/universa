/* istanbul ignore file */
import * as path from 'path';

export const INSTANCE = Symbol('instance');

export const DEFAULT_UMI_OPTIONS = Object.freeze({
  binaryPath: path.resolve(__dirname, '..', 'contrib', 'umi', 'bin', 'umi'),
  system: 'UMI',
  version: '~0.8.0',
});

export const DEFAULT_CLIENT_CREATE_OPTIONS = Object.freeze({
  topology: 'mainnet',
  cacheDir: null,
});
