/* istanbul ignore file */

export class PrivateConstructorError extends Error {
  constructor(constructorName: string) {
    super(`Constructor of class '${constructorName}' is private and only accessible within the class declaration.`);
  }
}

export class InterchangeError extends Error {
  constructor(message = "Objects can't be interchanged between different UMI interfaces") {
    super(message);
  }
}
