import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';

export class KeyAddress extends RemoteObject {
  [property: string]: any; // eslint-disable-line @typescript-eslint/no-explicit-any
  protected static readonly remoteClassName = 'com.icodici.crypto.KeyAddress';

  private _string?: string = undefined;

  public static async create(remoteObject: RemoteObjectRef): Promise<KeyAddress>;
  public static async create(...args: any[]): Promise<KeyAddress>; // eslint-disable-line @typescript-eslint/no-explicit-any
  public static async create(...args: any[]): Promise<KeyAddress> {
    return super.create.call(this, ...args) as Promise<KeyAddress>;
  }

  public static async fromPacked(data: Buffer): Promise<KeyAddress> {
    return KeyAddress.create(data);
  }

  public async toString(): Promise<string> {
    if (!this._string) {
      this._string = (await this.invoke('toString')) as string;
    }
    return this._string;
  }

  public async equals(another: string | KeyAddress): Promise<boolean> {
    if (this === another) return true;
    if (typeof another === 'string') {
      if (another.length === 37 || another.length === 53) {
        const packed = await this.getPacked();
        return packed === another;
      } else {
        const string = await this.toString();
        return string === another;
      }
    }
    return false;
  }
}

RemoteObject.registerProxy(KeyAddress);
