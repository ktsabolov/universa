import { PrivateKey } from './PrivateKey';
import { RemoteObject } from './RemoteObject';
import { RemoteObjectRef } from './RemoteObjectRef';
import { UMI } from './UMI';

export type RemoteObjectRefOptions = {
  umi: UMI;
  ref: UmiRef;
};

export type JsEntity =
  | Buffer
  | Date
  | RemoteObject
  | RemoteObjectRef
  | Set<RemoteObject>
  | Set<RemoteObjectRef>
  | string
  | number
  | boolean
  | null
  | undefined;

export interface JsObject {
  [index: string]: JsEntity | JsObject | JsArray;
}

export interface JsArray {
  [index: number]: JsEntity | JsObject | JsArray;
}

export type JsValue = JsEntity | JsArray | JsObject;

enum UmiTypes {
  RemoteObject = 'RemoteObject',
  unixtime = 'unixtime',
  binary = 'binary',
}

export type UmiRef = {
  __type: UmiTypes.RemoteObject;
  className?: string;
  id: number;
};

export type UmiTime = {
  __type: UmiTypes.unixtime;
  seconds: number;
};

export type UmiBinary = {
  __type: UmiTypes.binary;
  base64: string;
};

export type UmiScalar = null | boolean | number | string | UmiRef | UmiTime | UmiBinary;

export type UmiArray = Array<UmiScalar | UmiHash>;

export interface UmiHash {
  [index: string]: UmiScalar | UmiArray | UmiHash;
}
export type UmiValue = UmiScalar | UmiArray | UmiHash;

export type UmiOptions = {
  binaryPath: string;
  system: string;
  version: string;
};

export type UmiCreateOptions = Partial<UmiOptions>;

export type UmiVersion = {
  system: string;
  version: string;
};

export type UmiCall = {
  resolve(value?: unknown): void;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  reject(reason?: any): void;
};

export type UmiResponse = {
  ref: number;
  result?: { [key: string]: UmiValue };
  error: {
    text: string;
  };
};

interface BaseClientCreateOptions {
  cacheDir?: string;
  privateKey?: PrivateKey;
}

interface ClientCreateOptionsWithTopologyName extends BaseClientCreateOptions {
  topology?: string;
  topologyFile?: never;
}

interface ClientCreateOptionsWithTopologyFile extends BaseClientCreateOptions {
  topologyFile?: string;
  topology?: never;
}

export type ClientCreateOptions = ClientCreateOptionsWithTopologyName | ClientCreateOptionsWithTopologyFile;

export enum ItemStates {
  APPROVED = 'APPROVED',
  LOCKED = 'LOCKED',
  DECLINED = 'DECLINED',
  REVOKED = 'REVOKED',
  UNDEFINED = 'UNDEFINED',
  PENDING = 'PENDING',
  PENDING_POSITIVE = 'PENDING_POSITIVE',
  PENDING_NEGATIVE = 'PENDING_NEGATIVE',
  DISCARDED = 'DISCARDED',
  LOCKED_FOR_CREATION = 'LOCKED_FOR_CREATION',
}

export type UniversaContractState = {
  state: ItemStates;
  errors?: string[];
};
