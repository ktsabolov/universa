import { UniversaContractState } from './types';

export class ContractState {
  constructor(private readonly _source: UniversaContractState) {}

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private _errors?: any[];
  public get errors() {
    if (!this._errors) {
      this._errors = this._source.errors || [];
    }
    return this._errors;
  }

  public get hasErrors() {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    return !!this.errors!.length;
  }

  public get state() {
    return this._source.state;
  }

  public get isPending() {
    return this.state.startsWith('PENDING');
  }

  public get pending() {
    return this.isPending;
  }

  public get isApproved() {
    return ['APPROVED', 'LOCKED'].includes(this.state);
  }

  public get approved() {
    return this.isApproved;
  }
}
